---
stage: none
group: unassigned
description: Runners, jobs, pipelines, variables.
info: To determine the technical writer assigned to the Stage/Group associated with this page, see https://handbook.gitlab.com/handbook/product/ux/technical-writing/#assignments
---

# Use CI/CD to build your application

Use CI/CD to generate your application.

| | | |
|--|--|--|
| [**Getting started**](../ci/index.md)<br>Overview of how features fit together. | [**CI/CD YAML syntax reference**](../ci/yaml/index.md)<br>Pipeline definition, artifacts, debugging, examples, steps. | [**Runners**](https://docs.gitlab.com/runner/)<br>Execution, agents, job processing, installation, configuration. |
| [**Pipelines**](../ci/pipelines/index.md)<br>Configuration, automation, stages, jobs, schedules, efficiency. | [**Jobs**](../ci/jobs/index.md)<br>Configuration, logs, artifacts. | [**CI/CD components**](../ci/components/index.md)<br>Reusable, versioned CI/CD components for pipelines. |
| [**Variables**](../ci/variables/index.md)<br> Configuration, usage, security, troubleshooting. | [**Pipeline security**](../ci/pipelines/pipeline_security.md)<br>Secrets management, job tokens, secure files, cloud security. | [**Services**](../ci/services/index.md)<br>Reusable database or caching images. |
| [**Auto DevOps**](autodevops/index.md)<br>Automated DevOps, CI/CD, language detection, deployment, customization. | [**Testing**](../ci/testing/index.md)<br>Unit tests, integration tests, test reports, coverage, quality assurance. | [**SSH keys**](../ci/ssh_keys/index.md)<br>Authentication, secure access, deployment, remote execution, key management. |
| [**ChatOps**](../ci/chatops/index.md)<br>Collaboration, chat integration, commands, automation, communication. | [**Mobile DevOps**](../ci/mobile_devops.md)<br>Mobile apps, Android, build automation, app distribution. | [**Google cloud integration**](../ci/gitlab_google_cloud_integration/index.md)<br>Cloud services, Kubernetes deployments. |
| [**External repository integrations**](../ci/ci_cd_for_external_repos/index.md)<br>GitHub, Bitbucket, external sources, mirroring, cross-platform. | | |
